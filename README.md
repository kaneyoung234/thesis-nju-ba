# Thesis-NJU-BA

This is an undergraduate thesis LaTeX module based on the thesis requirement for undergraduate students at Nanjing University China.

1. Main part of this module is in the file main.tex.
2. Reference items are stored in the file ref.bib and the format of references have been adjusted on the boost file gbt7714-2005.bst, which is written based on Chinese National Standard GB/T 7714-2005. So you don't need to do anything to write reference at all.
3. Since to adjust the cover and abstract to meet the requirement is quite complex, I recommend you to write cover and abstract part with the .doc files provided by the university and convert them to .pdf files. In the directory, you can find a python3.4 script manipulation.py, which is used to merge the all the .pdf files, cover.pdf, abstract.pdf and main.pdf together and will generate a new file named merge.pdf, which is the final thesis we want.

Contact me with yanghan@uchicago.edu if you have any problem.
