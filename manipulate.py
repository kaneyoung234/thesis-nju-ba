#!/usr/local/bin/python3.4
from pdfrw import PdfReader, PdfWriter
ipdf = PdfReader("main.pdf")
wpdf = PdfReader("cover.pdf")

writer = PdfWriter()
writer.addpages(wpdf.pages)
writer.addpages(ipdf.pages)
writer.write("merge.pdf")
